package com.maxin.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	public void sendToMongo(String json) {
		logger.info("sending to MongoDB");
		MongoClient client = new MongoClient();
		MongoDatabase mondoDatabase = client.getDatabase("vendor");
		MongoCollection<Document> mongoCollection = mondoDatabase.getCollection("contact");
		logger.info("Converting JSON to DBObject");
		Document document = Document.parse(json);
		mongoCollection.insertOne(document);
		logger.info("sent to mongodb");
	}

}

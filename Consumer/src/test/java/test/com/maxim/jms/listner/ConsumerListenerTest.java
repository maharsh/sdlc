package test.com.maxim.jms.listner;

import static org.junit.Assert.*;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.maxin.jms.listener.ConsumerListner;

public class ConsumerListenerTest {

	private TextMessage message;
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOnMessage() {
		ConsumerListner listner =new ConsumerListner();
		listner.onMessage(message);
		assertNull(message);
	}

}
